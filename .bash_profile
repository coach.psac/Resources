export M2_HOME=/Users/zoazh/My\ Drive/apache-maven-3.5.2/
export PATH=$PATH:/Users/zoazh/My\ Drive/apache-maven-3.5.2/bin
export JAVA_HOME=$(/usr/libexec/java_home)
export JDK_HOME=$(/usr/libexec/java_home)
export PATH=$PATH:/usr/local/mysql/bin
export PATH="$PATH:/Applications/Visual Studio Code.app/Contents/Resources/app/bin"
export ANDROID_HOME=/Users/zoazh/Library/Android/sdk
export PATH=$PATH:$ANDROID_HOME/tools
export PATH=$PATH:$ANDROID_HOME/platform-tools

alias mfec="cd ~/Documents/Projects/MFEC/"
alias wise="cd ~/Documents/Projects/MFEC/bcs/functional-test/wise-mock"
alias scb="cd ~/Documents/Projects/MFEC/bcs/software-project/MashupService"

alias gh="cd ~/GitHub/"
alias spring="cd ~/GitHub/Spring/"
# Setting PATH for Python 2.7
# The original version is saved in .bash_profile.pysave
PATH="/Library/Frameworks/Python.framework/Versions/2.7/bin:${PATH}"
export PATH
alias mvnproxy="cd ~/My\ Drive/apache-maven-3.5.2/conf"